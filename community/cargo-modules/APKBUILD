# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=cargo-modules
pkgver=0.11.0
pkgrel=0
pkgdesc="A cargo plugin for showing an overview of a crate's modules"
url="https://github.com/regexident/cargo-modules"
# s390x: FTBFS
arch="all !s390x"
license="MPL-2.0"
makedepends="
	cargo
	cargo-auditable
	"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/regexident/cargo-modules/archive/$pkgver.tar.gz"
options="net" # needed to fetch crates

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen
}

package() {
	install -Dm 755 target/release/cargo-modules -t "$pkgdir"/usr/bin
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
}

sha512sums="
7a4d5c7837af8564bf7047f53548686a698efe326e062d2095c1664994b4fea3a62322f1c570b92d14d32551f9e714a1c26864f26b26c67b0315be2e46768888  cargo-modules-0.11.0.tar.gz
"
