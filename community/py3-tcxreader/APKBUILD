# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-tcxreader
_pkgorig=tcxreader
pkgver=0.4.5
pkgrel=0
pkgdesc="tcxparser is a reader / parser for Garmin’s TCX file format"
url="https://github.com/alenrajsp/tcxreader"
arch="noarch"
license="MIT"
depends="python3"
checkdepends="py3-pytest-xdist"
makedepends="py3-gpep517 py3-poetry-core"
subpackages="$pkgname-pyc"
source="https://github.com/alenrajsp/tcxreader/archive/v$pkgver/$_pkgorig-$pkgver.tar.gz"
builddir="$srcdir/$_pkgorig-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
5920023dd7a65b39009d96f218ecd982b5479756bd4dfb0482901cd04f6c6753a218d3d5a8c3de722b31eb0670526d5afc445e606978e3203b359c0c69c98b64  tcxreader-0.4.5.tar.gz
"
